<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Ajustes_sistema extends Panel{
        function __construct() {
            parent::__construct();
            $this->load->library('image_crud');
        }
        
        function ajustes(){
            $crud = $this->crud_function('','');
            $crud->field_type('ubicacion','map');
            $crud->unset_add()
                     ->unset_delete();
            $this->loadView($crud->render());
        }
        
        function banner(){
            $crud = $this->crud_function('','');
            $crud->set_field_upload('foto','images/banner');
            $this->loadView($crud->render());
        }
        
        function fotos(){
            $this->load->library('image_crud');
            $crud = new image_CRUD();
            $crud->set_table('fotos')
                    ->set_url_field('foto')
                    ->set_image_path('images/fotos')
                    ->set_ordering_field('priority')
                    ->module = 'admin';
            
            $this->loadView($crud->render());
        }                
        
        function provincias(){
            $crud = $this->crud_function('','');
            $crud->set_field_upload('foto','images/banner');
            $this->loadView($crud->render());
        }
        
        function notificaciones(){
            $crud = $this->crud_function('','');            
            $this->loadView($crud->render());
        }
        
        function ventas(){
            $crud = $this->crud_function('','');
            $crud->order_by('id','DESC');                        
            $crud->set_subject('Les teves compres');
            $crud->unset_add()
                ->unset_edit()
                ->unset_read()
                ->unset_export()
                ->unset_print()
                ->display_as('Datos de entrega')
                ->columns('procesado','je8701ad4.nombre','je8701ad4.apellido_paterno','je8701ad4.ciudad','je8701ad4.direccion','fecha_compra','productos','total','costo_envio');
            $crud->add_action('<i class="fa fa-edit"></i> Ver Productos','',base_url('admin/ajustes_sistema/ventas_detalles').'/');
            $crud->set_relation('user_id','user','{nombre}|{apellido_paterno}|{direccion}|{ciudad}');
            $crud->display_as('fecha_compra','Data de compra')
                 ->display_as('procesado','Pago')
                 ->display_as('productos','Productos')
                 ->display_as('je8701ad4.nombre','Nombre')
                 ->display_as('je8701ad4.apellido_paterno','Apellido')
                 ->display_as('je8701ad4.direccion','Dirección Entrega')
                 ->display_as('je8701ad4.ciudad','Ciudad')
                 ->display_as('provincias_id','Provincia');
            
            $crud->callback_column('je8701ad4.nombre',function($val,$row){
                return explode('|',$row->se8701ad4)[0];
            });
            $crud->callback_column('je8701ad4.apellido_paterno',function($val,$row){
                return explode('|',$row->se8701ad4)[1];
            });
            $crud->callback_column('je8701ad4.direccion',function($val,$row){
                return explode('|',$row->se8701ad4)[2];
            });
            $crud->callback_column('je8701ad4.ciudad',function($val,$row){
                return explode('|',$row->se8701ad4)[3];
            });
            
            $crud->callback_column('procesado',function($val,$row){
                switch($val){
                    case '-1': return '<span class="label label-danger">No procesado</span>'; break;
                    case '1': return '<a href="'.site_url('usuario/comprar/'.$row->id).'" class="label label-default">Pendiente</a>'; break;
                    case '2': return '<span class="label label-success">Procesado</span>'; break;
                }
            });
            
            $crud->callback_column('total',function($val,$row){
                return moneda($val+$row->costo_envio);
            });
            
            $crud->callback_column('costo_envio',function($val,$row){
                return moneda($row->costo_envio);
            });
            
            $crud->callback_column('productos',function($val,$row){
                get_instance()->db->join('productos','productos.id = ventas_detalles.productos_id');
                $ventas = get_instance()->db->get_where('ventas_detalles',array('ventas_id'=>$row->id));
                $str = '';
                foreach($ventas->result() as $v){
                    $str.= $v->cantidad.' '.$v->producto_nombre.',';
                }
                return $str;
            });
            
            $crud->callback_before_delete(function($id){
                get_instance()->db->delete('ventas_detalles',array('ventas_id'=>$id));
            });
            $output = $crud->render();
            $output->title = 'Les teves compres';
            $this->loadView($output);
        }
        
        function ventas_detalles($x = ''){
            if(is_numeric($x)){
                $crud = $this->crud_function('','');
                $crud->where('ventas_id',$x);
                $crud->order_by('id','DESC');                        
                $crud->set_subject('Les teves compres');
                $crud->columns('j4b04c546.producto_nombre','cantidad','monto','talla','color');
                $crud->set_relation('productos_id','productos','{producto_nombre}|{foto}');
                $crud->display_as('j4b04c546.producto_nombre','Producte');
                $crud->display_as('cantidad','Quantitat')
                     ->display_as('monto','Import');
                $crud->unset_add()
                     ->unset_edit()
                     ->unset_read()
                     ->unset_export()
                     ->unset_print()
                     ->unset_delete();  
                
                $crud->callback_column('j4b04c546.producto_nombre',function($val,$row){
                    $n = explode('|',$row->s4b04c546);
                    return '<a class="image-thumbnail" href="'.base_url('images/productos/'.$n[1]).'">'.img('images/productos/'.$n[1],'width:30px').'</a> '.$n[0];
                });
                
                $crud->callback_column('monto',function($val,$row){
                    return moneda($val);
                });
            
                $output = $crud->render();
                $output->title = 'Les teves compres';
                $this->loadView($output);
            }
        }
    }
?>
