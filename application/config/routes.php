<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['default_controller'] = "main";
$route['productos/(:any)']  = 'main/productosShow/$1';
$route['404_override'] = 'main/error404';
$route['403_override'] = 'main/error403';


/* End of file routes.php */
/* Location: ./application/config/routes.php */
