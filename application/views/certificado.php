<?php 
    $posiciones = json_decode($evento->posiciones);         
?>
<div style="background-image:url(<?= base_url('images/certificados/'.$evento->certificado) ?>); width:1024px; height:1024px; background-repeat:no-repeat">       
        <div style="position:absolute; top:<?= $posiciones->nombre->top ?>; left:<?= $posiciones->nombre->left ?>;" data-val="nombre">{nombre}</div>
        <div style="position:absolute; top:<?= $posiciones->apellido_paterno->top ?>; left:<?= $posiciones->apellido_paterno->left ?>;" data-val="apellido_paterno">{apellido_paterno}</div>
        <div style="position:absolute; top:<?= $posiciones->titulo->top ?>; left:<?= $posiciones->titulo->left ?>;" data-val='titulo'>{titulo}</div>
        <div style="position:absolute; top:<?= $posiciones->fecha->top ?>; left:<?= $posiciones->fecha->left ?>;" data-val='fecha'>{fecha}</div>
        <div style="position:absolute; top:<?= $posiciones->chip->top ?>; left:<?= $posiciones->chip->left ?>;" data-val='chip'>{chip}</div>
        <div style="position:absolute; top:<?= $posiciones->grupo->top ?>; left:<?= $posiciones->grupo->left ?>;" data-val='grupo'>{grupo}</div>
        <div style="position:absolute; top:<?= $posiciones->categoria->top ?>; left:<?= $posiciones->categoria->left ?>;" data-val='categoria'>{categoria}</div>
        <div style="position:absolute; top:<?= $posiciones->tiempo_oficial->top ?>; left:<?= $posiciones->tiempo_oficial->left ?>;" data-val='tiempo_oficial'>{tiempo_oficial}</div>
        <div style="position:absolute; top:<?= $posiciones->tiempo_chip->top ?>; left:<?= $posiciones->tiempo_chip->left ?>;" data-val='tiempo_chip'>{tiempo_chip}</div>
        <div style="position:absolute; top:<?= $posiciones->lugar_obtenido->top ?>; left:<?= $posiciones->lugar_obtenido->left ?>;" data-val='lugar_obtenido'>{lugar_obtenido}</div>
</div>