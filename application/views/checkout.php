<?php ini_set('display_errors',true); ?>
<?php $this->load->view('includes/headerMain') ?>
<?php $total = $producto->row()->total+3.50 ?>
<?php 
    // Se crea Objeto
    $miObj = new RedsysAPI;

    // Valores de entrada
    //$merchantCode 	="336711114" TEST;
    $merchantCode 	="336711114";
    //$key = 'sq7HjrUOBfKmC576ILgskD5srU870gJ7'; TEST
      $key = "27e/nS62I/5mImuK/9kaN9vtbyivaWGO";
    $terminal 		="1";
    $amount 		=$total*100;
    $currency 		="978";
    $transactionType    ="0";
    $merchantURL 	=base_url('main/procesarPago');
    $urlOK 		=base_url('main/pagoOk');
    $urlKO 		=base_url('main/pagoKo');
    $order 		='000'.$id;

    //Entorno
    //$urlPago = "https://sis.redsys.es/sis/realizarPago"; //ENTORNO REAL    

    // Se Rellenan los campos
    $miObj->setParameter("DS_MERCHANT_AMOUNT",$amount);
    $miObj->setParameter("DS_MERCHANT_ORDER",strval($order));
    $miObj->setParameter("DS_MERCHANT_MERCHANTCODE",$merchantCode);
    $miObj->setParameter("DS_MERCHANT_CURRENCY",$currency);
    $miObj->setParameter("DS_MERCHANT_TRANSACTIONTYPE",$transactionType);
    $miObj->setParameter("DS_MERCHANT_TERMINAL",$terminal);
    $miObj->setParameter("DS_MERCHANT_MERCHANTURL",$merchantURL);
    $miObj->setParameter("DS_MERCHANT_URLOK",$urlOK);		
    $miObj->setParameter("DS_MERCHANT_URLKO",$urlKO);
        
    $version="HMAC_SHA256_V1";
    //$key = 'xqSeBn4Qp+EsCTtko9MwNEGqFnMoDj2d';//Clave secreta del terminal    
    $request = "";
    $params = $miObj->createMerchantParameters();
    $signature = $miObj->createMerchantSignature($key);
?>
<div id="page" class="single single-product postid-1185 woocommerce woocommerce-page full-width">
    <div id="content" class="site-content">
    <section class="page-container">
        <main id="main" class="site-main"  role="main" itemprop="mainContentOfPage" >
            <div class="container">
                <article class="page type-page">
                    <div class="panel panel-default">
                        <div class="">
                            <?php if(!empty($producto)): ?>                                
                                
                                <div id="order_review">
                                    <table class="shop_table" style="width:70%; margin:0 auto;">
                                        <thead>
                                            <tr>
                                                <th><h3 id="order_review_heading">Tu pedido</h3></th>
                                            </tr>
                                            <tr>
                                                <th class="product-name">Producto</th>
                                                <th class="product-total"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($producto->result() as $p): ?>
                                                <tr class="cart_item">
                                                    <td class="product-name">
                                                        <?= img('img/fotos_productos/'.$p->foto,'width:119px') ?>
                                                        <?= $p->nombre_producto ?> <strong class="product-quantity">&times; <?= $p->cantidadProd ?></strong>
                                                    </td>
                                                    <td class="product-total">
                                                        <span class="amount"><?= $p->precio ?>€</span>
                                                        <a href="javascript:del(<?= $p->idDetalle ?>)"><i class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                            <?php endforeach ?>
                                            <tr class="cart_item">
                                                <td class="product-name">                                                    
                                                    <strong class="product-quantity">Gastos de Tramitación Bancaria</strong>
                                                </td>
                                                <td class="product-total">
                                                    <span class="amount">3.50€</span>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr class="order-total">
                                                <th></th>
                                                <td><strong>Total: <span class="amount"><?= $total ?>€</span></strong> </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <div id="payment" class="woocommerce-checkout-payment">
                                        <?php //https://sis-t.redsys.es:25443/sis/realizarPago ?>
                                        <form action="https://sis.redsys.es/sis/realizarPago" method="post">
                                            
                                            <?php $total = $total*100; ?>                                            
                                            <input type="hidden" name="Ds_SignatureVersion" value="<?php echo $version; ?>"/>
                                            <input type="hidden" name="Ds_MerchantParameters" value="<?php echo $params; ?>"/>
                                            <input type="hidden" name="Ds_Signature" value="<?php echo $signature; ?>" />
                                            <div class="form-row place-order" align="center">
                                                <?= img('img/pay.jpg') ?><br/>
                                                <button type="submit" id="buttonPay" class="button">Pagar</button>
                                                <div style=" margin-top: 20px;margin-bottom: 10px"><input type="checkbox" name="licencia" required=""> <a href="javascript:mostrarlic()">Política de venta</a> y condiciones generales</div>
                                                <div style=" margin-bottom: 10px;font-size: 13px">Finalia Viajes, S.L. / C. Girona, 34 / 08700 Igualada / España / <a href="mailto:reservas@gandiabeachfestival.com">reservas@gandiabeachfestival.com / <a href="tel:902002068">902 002 068</div>
                                            </div>
                                        </form>
                                        <div class="clear"></div>
                                        <div class ="alert alert-danger" style="display:none">La compra ha sido eliminada, por lo que ya no puede procesarse</div>
                                    </div>                                                    
                                </div>
                            <?php else: ?>
                            No hay productos añadidos al carrito
                            <?php endif ?>
                        </div>                                        
                    </div><!-- .entry-content -->
                </article><!-- #post -->
            </div>
        </main><!-- #main -->
    </section><!-- .container -->
    </div><!-- #content -->
<?php $this->load->view('includes/footer') ?>
</div><!-- #page -->
<script>
    function del(){
        $.post('<?= base_url('usuario/compras/delete/'.$id) ?>',{id:'<?= $id ?>'},function(data){
            data = JSON.parse(data);
            if(data.success){
                $(".alert").show();
                $('#buttonPay').hide();
            }
                
        });
    }
    
    function mostrarlic(){
        $.post('<?= base_url('main/mostrarlic') ?>',{},function(data){
            emergente(data);
        });
    }
    
    function validar(){
        if($("#licencia").prop('checked')){
            return true;
        }else{
            alert('Debe aceptar los terminos antes de continuar');
        }
        return false;
    }
</script>
