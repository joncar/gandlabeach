<header id="header" class="awe-menubar-header">
    <nav class="awemenu-nav headroom" data-responsive-width="1200">
        <div class="container">
            <div class="awemenu-container">

                <div class="navbar-header">
                    <ul class="navbar-icons">

                        <?php if(!empty($_SESSION['user'])): ?>
                        <li class="menubar-account">
                            <a href="<?= site_url('panel') ?>" title="" class="awemenu-icon">
                                <i class="icon icon-user-circle"></i>
                                <span class="awe-hidden-text">Cuenta</span>
                            </a>

                            <ul class="submenu megamenu">
                                <li>
                                    <div class="container-fluid">
                                        <div class="header-account" style="text-align: center; width: 64%;">
                                            <div class="header-account-avatar">
                                                <a href="#" title="">
                                                    <img class="img-circle" style="width:60%;" src="<?= base_url(empty($this->user->foto)?'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png':'img/fotos/'.$this->user->foto) ?>" alt="<?= $this->user->nombre ?>" />
                                                </a>
                                            </div>

                                            <div class="header-account-username">
                                                <h4><a href="#"><?= $this->user->nombre ?></a></h4>
                                            </div>

                                            <ul>
                                                <li><a href="<?= base_url('seguridad/perfil/edit/'.$this->user->id) ?>">Perfil d'usuari</a>
                                                </li>
                                                <li><a href="<?= base_url('main/unlog') ?>">Sortir</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <?php else: ?>
                        <li class="menubar-account">
                            <a href="<?= site_url('registro/index/add') ?>" title="" class="awemenu-icon">
                                <i class="icon icon-user-circle"></i>
                                <span class="awe-hidden-text">Cuenta</span>
                            </a>
                        </li>
                        <?php endif ?>
                        <li class="menubar-cart">
                            <?php $this->load->view('includes/fragmentos/carritonav') ?>
                        </li>


                    </ul>

                    <ul class="navbar-search">
                        <li>
                            <a href="#" title="" class="awemenu-icon awe-menubar-search" id="open-search-form">
                                <span class="sr-only">Buscar</span>
                                <span class="icon icon-search"></span>
                            </a>

                            <div class="menubar-search-form" id="menubar-search-form">
                                <form action="<?php base_url('main/index') ?>" method="GET">
                                    <input type="search" name="producto" class="form-control" placeholder="Escriu la teva recerca aquí">
                                    <div class="menubar-search-buttons">
                                        <button type="submit" class="btn btn-sm btn-white">Cercar</button>
                                        <button type="button" class="btn btn-sm btn-white" id="close-search-form">
                                            <span class="icon icon-remove"></span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.menubar-search-form -->
                        </li>
                    </ul>

                </div>

                <div class="awe-logo">
                    <a href="<?= site_url() ?>" title="">
                        <?= img('img/logo.png','width:auto; height:130px; margin-bottom: 30px;',TRUE,'class="logo1 visible-xs visible-sm visible-md"') ?>
                        <?= img('img/logo.png','max-width:273px;',TRUE,'class="logo1 hidden-xs hidden-sm hidden-md"') ?>
                        <?= img('img/logo2.png','max-width:273px;',TRUE,'class="logo2 hidden-xs hidden-sm hidden-md hidden"') ?>
                    </a>
                </div>
                <!-- /.awe-logo -->


                <ul class="awemenu awemenu-right">
                    <?php foreach($this->querys->getCategorias()->result() as $c): ?>
                    <li class="awemenu-item">
                        <a href="<?= base_url('categorias/'.toURL($c->categoria_nombre).'-'.$c->id) ?>" title="">
                            <span><?= $c->categoria_nombre ?></span>
                        </a>     
                        <ul class="awemenu-submenu awemenu-megamenu" data-animation="fadeup">
                            <li class="awemenu-megamenu-item" style="padding:0px;">
                                <div class="container-fluid">
                                    <div class="awemenu-megamenu-wrapper" style="padding:0px; margin-top:-40px;">
                                        <ul class="sublist">
                                            <?php foreach($this->db->get_where('productos',array('categorias_id'=>$c->id))->result() as $p): ?>
                                                <li><a href="<?= site_url('productos/'. toURL($p->producto_nombre).'-'.$p->id) ?>" title=""><?= $p->producto_nombre ?></a></li>
                                            <?php endforeach ?>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <?php endforeach ?>
                </ul>
                <!-- /.awemenu -->
            </div>
        </div>
        <!-- /.container -->

    </nav>
    <!-- /.awe-menubar -->
    </header>
    <!-- /.awe-menubar-header -->