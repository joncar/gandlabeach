<footer class="footer">
<div class="footer-wrapper">
    <div class="footer-widgets">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12 col-sm-6">

                            <div class="widget">
                                <h3 class="widget-title">INFORMACIÓ SOBRE EL LLIURAMENT</h3>

                                <div class="widget-content">
                                    <p>Entreguem els dimecres i dijous de la setmana següent a la data de la comanda. </br>Per tant, el plaç d’entrega és de minim 3 dies i màxim 9, en funció del dia de la comanda.</p>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12 col-sm-6">

                            <div class="widget">
                                <h3 class="widget-title">TENS ALGÚN DUBTE?</h3>

                                <div class="widget-content">
                                    <p>Tel: 690 608 194</p>
                                    <p>Horari Consultes: 09:00-21:00</p>
                                    <p>Email: carndequalitat@canmabres.com</p>
                                </div>
                            </div>
                            <!-- /.widget -->

                        </div>
                    </div>
                </div>

                <div class="col-md-2 col-sm-6">

                    <div class="widget">
                        <h3 class="widget-title">LA TEVA INFO</h3>

                        <ul>
                            <li><a href="<?= base_url('main/carrito') ?>" title="">El Teu Carret</a>
                            </li>
                            <li><a href="<?= base_url('usuario/ventas') ?>" title="">Les Teves Comandes</a>
                            </li>
                        	<li><a href="<?= base_url('main/unlog') ?>" title="">Trancar sessió</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.widget -->

                </div>

                <div class="col-md-2 col-sm-6">

                    <div class="widget">
                        <h3 class="widget-title">MÉS LINKS</h3>

                        <ul>
                            <li><a href="<?= base_url('categorias/lots-vedella-1') ?>" title="">Tots els Lots</a>
                            </li>
                            <li><a href="<?= base_url('categorias/productes-vedella-2') ?>" title="">Productes de Vedella</a>
                            </li>
                            <li><a href="<?= base_url('categorias/productes-xai-3') ?>" title="">Productes de Xai</a>
                            </li>
                            <li><a href="<?= base_url('categorias/productes-gourmet-4') ?>" title="">Productes Gourmet</a>
                            </li>
                            <li><a href="http://www.canmabres.com" title="">Can Mabres web</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.widget -->

                </div>

                <div class="col-md-3">

                    <div class="widget">
                        <h3 class="widget-title">REDS SOCIALS</h3>

                        <ul class="list-socials">
                            <li><a href="https://twitter.com/canmabres" title=""><i class="icon icon-twitter"></i></a>
                            </li>
                            <li><a href="https://www.facebook.com/canmabres?fref=ts" title=""><i class="icon icon-facebook"></i></a>
                            </li>
                            <li><a href="https://plus.google.com/114442606795422822032/posts" title=""><i class="icon icon-google-plus"></i></a>
                            </li>
                            <li><a href="https://www.youtube.com/user/canmabres" title=""><i class="fa fa-youtube"></i></a>
                            </li>
                        </ul>
                    </div>



                    <div class="widget">
                        <h3 class="widget-title">TIPUS DE PAGAMENT</h3>

                        <ul class="list-socials">
                            <li>
                                <a title="transferència"> 
                                    <i class="fa fa-bank"></i> <span style="font-size:14px">   Transferència</span>
                                </a>
                            </li>
                            </br>

                            <li>
                                <a title="Targeta de crèdit">
                                    <i class="fa fa-credit-card"></i> <span style="font-size:14px">   Targeta de crédit</span>
                                </a>
                            </li>
  							</br>
                            <li>
                                <a title="Contrareembols">
                                    <i class="glyphicon glyphicon-home"></i> <span style="font-size:14px">   Contrareembols</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.widget -->

                </div>
            </div>
        </div>


    </div>
    <!-- /.footer-widgets -->

    <div class="footer-copyright">
        <div class="container">
            <div class="copyright">
                <p>Copyright &copy; 2015 Can Mabres - Telèfon: 690 608 194 - Can Mabres s/n - Castellolí - 08719 - Barcelona</p>
            </div>

            <div class="footer-nav">
                <nav>
                    <ul>
                        <li><a href="mailto:carndequalitat@canmabres.com" title="">Contacta</a>
                        </li>
                        <li><a href="javascript:legal()" title="">Avís legal</a>
                        </li>
                    </ul>
                </nav>                
            </div>
        </div>
    </div>
    <!-- /.footer-copyright -->
</div>
<!-- /.footer-wrapper -->

<a href="#" class="back-top" title="">
    <span class="back-top-image">
        <img src="<?= base_url('img/back-top.png') ?>" alt="">
    </span>

    <small>Anar a dalt</small>
</a>
<!-- /.back-top -->
</footer>
<script>
    function legal(){
        $.post('<?= base_url() ?>main/mostrarlic',{},function(data){
                 emergente(data);
        });
    }
</script>