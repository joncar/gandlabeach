<div class="white-popup product-quickview-popup">
    <div class="product">
        <div class="col-xs-4 col-sm-4">
            <div class="">
                <div style="padding:10px;" align="center">
                    <?= img('images/productos/'.$producto->foto,'width:100%') ?>
                </div>                
            </div>
        </div>
        <!-- /.product-media -->

        <div class="col-xs-12 col-sm-8" style="padding-bottom:40px; padding-top:40px">
            <h2>
                <a href="<?= site_url('productos/'. toURL($producto->producto_nombre).'-'.$producto->id) ?>" title="<?= $producto->producto_nombre ?>"> <?= $producto->producto_nombre ?></a>
            </h2>
            <!-- /.product-name -->

            <div>
                <span class="amount"><?= moneda($producto->precio) ?></span>
            </div>
            <!-- /.product-price -->

            <div style="white-space: pre-line;">
                <?= strip_tags($producto->descripcion) ?>
            </div>                       

            <div>
                <a href="javascript:addToCart('<?= $producto->id ?>',1)" class="btn btn-lg btn-primary">Afegir al carret</a>
            </div>
                <!-- /.product-actions -->
        </div>            

        </div>
        <!-- /.product-body -->
    </div>
</div>

<script>
    $(function() {
        $('.product-quickview-slider').owlCarousel({
            items: 1,
            nav: true,
            dots: false
        });
    });
</script>
