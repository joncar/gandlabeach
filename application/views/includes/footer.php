<footer id="colophon" class="site-footer"  role="contentinfo" itemscope="itemscope" itemtype="http://schema.org/WPFooter" >
<div class="left-footer">
    <div class="site-branding">
        <a class="site-logo" href="<?= site_url() ?>" rel="home">
            <img src="<?= base_url('img/logo.png') ?>" style="width:100%; height:auto;">
        </a>
    </div>

    <div class="bottom-info">
        <?php if(empty($_SESSION['user'])): ?>
        <div class="login-register">Hola invitado! <a id="wr-login" href="<?= site_url('panel') ?>">Entrar</a> o <a id="wr-register" href="<?= site_url('panel') ?>">Regístrate</a></div>	
        <?php else: ?>
        <div class="login-register">Hola <?= $this->user->nombre ?> <a id="wr-login" href="<?= site_url('panel') ?>">Entra a tu cuenta</a></div>	
        <?php endif ?>
    </div>
</div><!-- end left footer -->
<div class="right-footer">
    <div class="right-footer-content">
        <div class="currency">
            <ul>
                <li><a href="<?= $this->db->get('ajustes')->row()->facebook ?>"><i class="fa fa-facebook"></i></a></li>
                <li><a href="<?= $this->db->get('ajustes')->row()->twitter ?>"><i class="fa fa-youtube-square"></i></a></li>
                <li><a href="<?= $this->db->get('ajustes')->row()->instagram ?>"><i class="fa fa-instagram"></i></a></li>
                <li><a href="<?= $this->db->get('ajustes')->row()->linkedin ?>"><i class="fa fa-soundcloud"></i></a></li>
            </ul>
        </div>
    </div>
    <div class="right-footer-content">
        <div class="currency">
            <ul>
                <li>
                    <a href="http://www.mallorcaislandfestival.com/tienda/">
                        <img src="<?= base_url('img/banner-footer.jpeg') ?>" style="width:360px; height:95px;">
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div><!-- end right footer -->
</div><!-- end more inner footer -->
</footer><!-- #colophon -->
<div id="mask-blur"></div>
<div id="modal" style="background:rgba(0,0,0,.9); width:100%; height:100%; position:fixed; top:0px; left:0px; padding:50px; display:none; z-index:10000">
<div style="background:white; height:auto; padding:20px; overflow:auto; max-height:600px;">
    <div align="right" style="font-size:24px;">      	
    <a href="javascript:closeModal()">Seguir comprando</a>
  </div>
  <div id="contenidoModal">Contenido</div>
</div>
</div>
<script>
function addCart(){
    var prod = $("#producto").val();
    var cant = $("#cantidad").val();
    $.get('<?= base_url('main/addToCart') ?>/'+prod+'/'+cant,{},function(data){
        $("#contenidoModal").html(data);
        $("#modal").show();
    });
    return false;
}

function showCart(){
    $.get('<?= base_url('main/showCart') ?>',{},function(data){
        $("#contenidoModal").html(data);
        $("#modal").show();
    });
    return false;
}

function remToCart(id){
    $.get('<?= base_url('main/delToCart') ?>/'+id,{},function(data){
        $("#contenidoModal").html(data);
        $("#modal").show();
    });    
}

function closeModal(){
    $("#modal").hide();
}

$(document).on('click','.showCartButton',function(){
   showCart();
});
</script>
