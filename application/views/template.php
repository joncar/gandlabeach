<!DOCTYPE html>
<html lang="en-US">
<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= empty($title)?'Gandia Beach Festival':$title ?></title>       
        <link rel='stylesheet' id='jquery-ui-custom-css'  href='<?= base_url()?>css/jquery-ui.css?ver=4.2.4' type='text/css' media='all' />
        <link rel='stylesheet' id='ninja-google-font-Raleway-css'  href='http://fonts.googleapis.com/css?family=Raleway%3A100%2C300%2C400%2C600%2C700%2C800%2C900&#038;subset=latin%2Cgreek-ext%2Ccyrillic%2Clatin-ext%2Cgreek%2Ccyrillic-ext%2Cvietnamese&#038;ver=4.2.4' type='text/css' media='all' />
        <link rel='stylesheet' id='owl-carousel-css'  href='<?= base_url()?>css/owl.carousel.css?ver=4.2.4' type='text/css' media='all' />
        <link rel='stylesheet' id='codyhouse-css'  href='<?= base_url()?>css/codyhouse.css?ver=4.2.4' type='text/css' media='all' />
        <link rel='stylesheet' id='dashicons-css'  href='<?= base_url()?>css/dashicons.min.css?ver=4.2.4' type='text/css' media='all' />
        <link rel='stylesheet' id='ninja-main-css'  href='<?= base_url()?>css/ninja/main.css?ver=4.2.4' type='text/css' media='all' />
        <link rel='stylesheet' id='ninja-custom-css'  href='<?= base_url()?>css/custom.css?ver=4.2.4' type='text/css' media='all' />
        <link rel='stylesheet' id='ninja-wcm-css'  href='<?= base_url()?>css/wcm.css?ver=4.2.4' type='text/css' media='all' />
        <link rel='stylesheet' id='ninja-responsive-css'  href='<?= base_url()?>css/responsive.css?ver=4.2.4' type='text/css' media='screen' />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <link rel='stylesheet' id='woocommerce-layout-css'  href='<?= base_url()?>css/woocommerce-layout.css?ver=2.3.13' type='text/css' media='all' />
        <link rel="shortcut icon" href="<?= base_url('img/favicon.ico') ?>">
        <?php if($this->router->fetch_method()=='productosShow'): ?>
        <script type='text/javascript' src='<?= base_url()?>js/jquery.1.8.2.min.js?ver=4.2.4'></script>
        <?php else: ?>
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>		
        <?php endif ?>
        <script type='text/javascript' src='<?= base_url()?>js/jquery.themepunch.tools.min.js?ver=4.6.3'></script>
        <script type='text/javascript' src='<?= base_url()?>js/jquery.themepunch.revolution.min.js?ver=4.6.3'></script>
        <script type='text/javascript' src='<?= base_url()?>js/resizable.min.js?ver=1.11.4'></script>
        <script type='text/javascript' src='<?= base_url()?>js/button.min.js?ver=1.11.4'></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <script type='text/javascript' src='<?= base_url()?>js/frame.js'></script>
        <style>
            .btn-success:hover, .btn-success:focus, .btn-success.focus, .btn-success:active, .btn-success.active, .open > .dropdown-toggle.btn-success {
                background-color: black !important;
                border-color: black !important;
                color: #ffffff;
            }
            .btn-success:hover, .btn-success:active, .open > .btn-success.dropdown-toggle {
                 background-color: black !important;
                border-color: black !important;
                color: #ffffff;
            }
            body, #page {
                background-color: #efefef;
                color: #353533;
            }
        </style>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                    // CUSTOM AJAX CONTENT LOADING FUNCTION
                    var ajaxRevslider = function(obj) {

                            // obj.type : Post Type
                            // obj.id : ID of Content to Load
                            // obj.aspectratio : The Aspect Ratio of the Container / Media
                            // obj.selector : The Container Selector where the Content of Ajax will be injected. It is done via the Essential Grid on Return of Content

                            var content = "<h2>THIS IS SOME TITLE</h2><br/>";

                            content += "Type:"+obj.type+"</br>";
                            content += "ID:"+obj.id+"</br>";        
                            content += "Aspect Ratio:"+obj.aspectratio+"</br>";  

                            data = {};

                            data.action = 'revslider_ajax_call_front';
                            data.client_action = 'get_slider_html';
                            data.token = '1acea51651';
                            data.type = obj.type;
                            data.id = obj.id;
                            data.aspectratio = obj.aspectratio;

                            // SYNC AJAX REQUEST
                            jQuery.ajax({
                                    type:"post",
                                    url:"admin-ajax.php",
                                    dataType: 'json',
                                    data:data,
                                    async:false,
                                    success: function(ret, textStatus, XMLHttpRequest) {
                                            if(ret.success == true)
                                                    content = ret.data;								
                                    },
                                    error: function(e) {
                                            console.log(e);
                                    }
                            });

                             // FIRST RETURN THE CONTENT WHEN IT IS LOADED !!
                             return content;						 
                    };

                    // CUSTOM AJAX FUNCTION TO REMOVE THE SLIDER
                    var ajaxRemoveRevslider = function(obj) {
                            return jQuery(obj.selector+" .rev_slider").revkill();
                    }

                    // EXTEND THE AJAX CONTENT LOADING TYPES WITH TYPE AND FUNCTION
                    var extendessential = setInterval(function() {
                            if (jQuery.fn.tpessential != undefined) {
                                    clearInterval(extendessential);
                                    if(typeof(jQuery.fn.tpessential.defaults) !== 'undefined')
                                            jQuery.fn.tpessential.defaults.ajaxTypes.push({type:"revslider",func:ajaxRevslider,killfunc:ajaxRemoveRevslider,openAnimationSpeed:0.3});   
                                            // type:  Name of the Post to load via Ajax into the Essential Grid Ajax Container
                                            // func: the Function Name which is Called once the Item with the Post Type has been clicked
                                            // killfunc: function to kill in case the Ajax Window going to be removed (before Remove function !
                                            // openAnimationSpeed: how quick the Ajax Content window should be animated (default is 0.3)
                            }
                    },30);
            });
        </script>
</head>

<body class="home page full-width" >
    <div id="pageloader">
        <div id="loader"></div>
        <div class="loader-section left"></div>
        <div class="loader-section right"></div>
    </div>

    <?php $this->load->view($view) ?>
    <script type='text/javascript' src='<?= base_url()?>js/modernizr.custom.79639.js?ver=4.2.4'></script>
    <script type='text/javascript' src='<?= base_url()?>js/slider-vertical.js?ver=4.2.4'></script>
    <script type='text/javascript' src='<?= base_url()?>js/jquery.blockUI.min.js?ver=2.60'></script>
    <script type='text/javascript' src='<?= base_url()?>js/jquery.cookie.min.js?ver=1.4.1'></script>
    <script type='text/javascript' src='<?= base_url()?>js/jquery.prettyPhoto.min.js?ver=3.1.5'></script>
    <script type='text/javascript' src='<?= base_url()?>js/jquery.prettyPhoto.init.min.js?ver=2.3.13'></script>
    <script type='text/javascript' src='<?= base_url()?>js/jquery.selectBox.min.js?ver=4.2.4'></script>
    <script type='text/javascript' src='<?= base_url()?>js/modernizr.js?ver=4.2.4'></script>
    <script type='text/javascript' src='<?= base_url()?>js/codyhouse.js?ver=4.2.4'></script>
    <script type='text/javascript' src='<?= base_url()?>js/owl.carousel.min.js?ver=4.2.4'></script>
    <script type='text/javascript' src='<?= base_url()?>js/placeholders.min.js?ver=4.2.4'></script>
    <script type='text/javascript' src='<?= base_url()?>js/velocity.min.js'></script>
    <script type='text/javascript' src='<?= base_url()?>js/main.js?ver=4.2.4'></script>        
    </body>
</html>

